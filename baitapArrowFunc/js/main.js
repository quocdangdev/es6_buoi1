const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

const onClickSetColor = (color) => {
    const elm = document.getElementById('house')
    elm.setAttribute('class', `house ${color}`)
}

const listColor = () => {
    const colors = colorList.map(color => `
    <button type="button"onClick="onClickSetColor('${color}')" class="color-button ${color}" ></button>
    `
    );
    const elm = document.getElementById("colorContainer")
    elm.innerHTML = colors.join(' ')
    document.getElementById('colorContainer').appendChild(elm)
}
listColor();

